/*!
An extremely simple isotope REPL, or rather RNPL (read, normalize, print, loop),
as normalization and evaluation are equivalent in the constant case
*/
use rustyline::{
    error::ReadlineError,
    Editor
};
use nom::{
    IResult,
    bytes::complete::tag,
    character::complete::multispace0,
    sequence::preceded,
    combinator::{opt, map},
    branch::alt
};

use isotope::{
    parser::{
        Parser,
        ParserMetadata,
        syntax::parse_expr
    },
    context::{
        Context
    }
};

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Command {
    Je,
    Jec,
    Eval,
    Quit
}

pub fn command(input: &str) -> IResult<&str, Command> {
    preceded(multispace0,
        alt((
            preceded(tag(":"), alt((
                map(tag("je"), |_| Command::Je),
                map(tag("jec"), |_| Command::Jec),
                map(tag("quit"), |_| Command::Quit)
            ))),
            map(opt(tag(":eval")), |_| Command::Eval)))
    )(input)
}

const CTX_VAL_CAP: usize = 100;
const CTX_DEP_CAP: usize = 100;
const PRS_VAR_CAP: usize = 50;

fn exec_line<'a>(parser: &mut Parser, line: &'a str) -> Result<&'a str, ()> {
    use Command::*;
    let (rest, command) = command(line).unwrap();
    match command {
        Je | Jec => {
            /*
            let (rest, v1) = parser.parse(rest).map_err(|err| {
                eprintln!("PARSE ERROR (Part #1 of JE): {:?}", err);
            })?;
            let (rest, v2) = parser.parse(rest).map_err(|err| {
                eprintln!("PARSE ERROR (Part #2 of JE): {:?}", err);
            })?;
            let jer = if command == Jec {
                parser.ctx().je(v1, v2)
            } else {
                parser.builder_mut().je(v1, v2)
            };
            match jer {
                Ok(c) => println!(
                    "YES: %{} is the CA of %{} and %{}", c.0.index(), v1.0.index(), v2.0.index()
                ),
                Err((r1, r2)) => println!(
                    "NO: %{} -> %{} and %{} -> %{} have no CA",
                    v1.0.index(), r1.0.index(), v2.0.index(), r2.0.index()
                )
            }
            Ok(rest)
            */
            unimplemented!()
        },
        Eval => {
            let (rest, expr) = parse_expr(rest).map_err(|err| {
                eprintln!("SYNTAX ERROR (EVAL): {:?}", err)
            })?;
            let val = parser.parse(&expr).map_err(|err| {
                eprintln!("SEMANTIC ERROR (EVAL): {:?}", err);
            })?;
            //TODO: recursive display
            println!("{}", parser.ctx()[val]);
            Ok(rest)
        },
        Quit => {
            std::process::exit(0)
        }
    }
}

fn main() {
    let mut rl = Editor::<()>::new();
    let mut ctx = Context::<ParserMetadata>::with_capacity(CTX_VAL_CAP, CTX_DEP_CAP);
    let mut parser = Parser::with_ctx(&mut ctx, PRS_VAR_CAP);
    loop {
        let line = rl.readline("isotope> ");
        match line {
            Ok(line) => {
                rl.add_history_entry(line.as_str());
                let mut line = line.as_str();
                loop {
                    match exec_line(&mut parser, line) {
                        Err(_) | Ok("") => break,
                        Ok(rest) => { line = rest }
                    }
                }
            },
            Err(ReadlineError::Eof) => {
                eprintln!("EOF");
                break
            },
            Err(err) => {
                eprintln!("READLINE ERROR: {}", err);
                break
            }
        }
    }
}
