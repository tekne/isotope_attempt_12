/*!
The isotope programming language
*/

pub mod parser;
pub mod context;
pub mod builder;

/// The index for native compiler-supported tuple types.
pub type TupleIx = u32;
/// The index to use for value definitions
pub type ValIx = usize;
/// The index to use for value definition generations
pub type ValGenIx = typed_generational_arena::IgnoreGeneration;
