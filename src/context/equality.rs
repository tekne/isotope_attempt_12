/*!
Methods for testing judgemental equality in a context
*/
use super::{
    Context,
    ValId
};

impl <M> Context<M> {
    /// Get the judgemental equality representative of a value
    pub fn eq_repr(&self, mut v: ValId) -> ValId {
        while self[v].eq != v {
            v = self[v].eq
        }
        v
    }
}
