/*!
Substitution expressions
*/
use hashbrown::hash_map::HashMap;
use std::convert::TryInto;

use super::{
    Context,
    ValId,
    DefId
};

pub enum SubstitutionError {

}

impl<M> Context<M> {
    /// Substitute one set of values for another in a value's definition.
    /// Return the value ID of the substituted value
    pub fn substitute(
        &mut self,
        substitutions: &mut HashMap<ValId, ValId>,
        target: ValId,
        label: DefId
    ) -> Result<ValId, SubstitutionError> {
        // Trivial case: value is to be substituted.
        if let Some(sub) = substitutions.get(&target) { return Ok(*sub) }
        // Recursively substitute all dependencies of the target. Check for differences.
        //TODO: think about a cursor based interface to save memory...
        let mut deps: Vec<ValId> = self[target].referenced().collect();
        let mut diff = false;
        for dep in deps.iter_mut() {
            // Cut off branches sure not to be dependent on the parameters being substituted, if any
            if let Ok(label) = label.try_into() {
                if !self[*dep].maybe_parametrized(label) { continue }
            }
            // Skip the self case. This will be handled later if there are differences
            if *dep == target { continue }
            // Now, try to substitute recursively
            let substitution_result = self.substitute(substitutions, *dep, label)?;
            // If substitution yields a different result, record there was a difference
            if substitution_result != *dep {
                diff = true;
                *dep = substitution_result;
            }
        }
        // If there is no difference, return the target, after setting it to map to itself
        if !diff {
            substitutions.insert(target, target);
            return Ok(target)
        }
        // If there *is* a difference, create the new, substituted value
        unimplemented!()
    }
}
