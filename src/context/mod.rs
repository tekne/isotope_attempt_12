/*!
Typing contexts and functions for analyzing and manipulating them
*/
use internship::IStr;
use hashbrown::hash_map::HashMap;
use smallvec::SmallVec;
use derive_more::{From, Into, Index, IndexMut};
use std::ops::{Index, IndexMut};
use std::default::Default;
use std::convert::TryFrom;
use enumset::{EnumSet, EnumSetType};

use crate::{TupleIx, ValIx, ValGenIx};

pub mod display;
pub mod substitution;
pub mod equality;

type ValArena<Sym=IStr, Mem=IStr> = typed_generational_arena::Arena<Value<Sym, Mem>, ValIx, ValGenIx>;
type ValArenaIx<Sym=IStr, Mem=IStr> = typed_generational_arena::Index<Value<Sym, Mem>, ValIx, ValGenIx>;

/// The ID of a valid isotope value
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct DefId(ValArenaIx);

impl Index<DefId> for ValArena {
    type Output = Value;
    #[inline(always)] fn index(&self, id: DefId) -> &Value { self.index(id.0) }
}

impl IndexMut<DefId> for ValArena {
    #[inline(always)] fn index_mut(&mut self, id: DefId) -> &mut Value { self.index_mut(id.0) }
}

impl Default for DefId {
    /// Get the root index, which is treated specially in the value graph
    #[inline] fn default() -> Self {
        DefId(ValArenaIx::from_idx_first_gen(0))
    }
}

/// The ID of an isotope value determined to be a type *and* non-root
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct TypeId(ValArenaIx);

impl TypeId {
    /// Assert a value should be a valid type. If this turns out to be wrong,
    /// type checking may fail (which is desired)
    pub fn assert_type(v: ValId) -> TypeId { TypeId(v.0) }
}

/// The ID of an isotope value determined to be a definition *and* non-root
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct ValId(ValArenaIx);

impl Index<TypeId> for ValArena {
    type Output = Value;
    #[inline(always)] fn index(&self, id: TypeId) -> &Value { self.index(id.0) }
}

impl IndexMut<TypeId> for ValArena {
    #[inline(always)] fn index_mut(&mut self, id: TypeId) -> &mut Value { self.index_mut(id.0) }
}

impl Index<ValId> for ValArena {
    type Output = Value;
    #[inline(always)] fn index(&self, id: ValId) -> &Value { self.index(id.0) }
}

impl IndexMut<ValId> for ValArena {
    #[inline(always)] fn index_mut(&mut self, id: ValId) -> &mut Value { self.index_mut(id.0) }
}


impl From<ValId> for DefId {
    #[inline] fn from(df: ValId) -> DefId { DefId(df.0) }
}

/// The error that a value ID is root and hence cannot be a definition ID
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct IsRoot;

impl TryFrom<DefId> for ValId {
    type Error = IsRoot;
    fn try_from(v: DefId) -> Result<ValId, IsRoot> {
        if v == DefId::default() { Err(IsRoot) } else { Ok(ValId(v.0)) }
    }
}

impl From<TypeId> for DefId {
    #[inline] fn from(ty: TypeId) -> DefId { DefId(ty.0) }
}

impl From<TypeId> for ValId {
    #[inline] fn from(ty: TypeId) -> ValId { ValId(ty.0) }
}

impl ValId {
    /// Create an invalid ValID. For module-internal use only, for now
    fn invalid() -> ValId { ValId(DefId::default().0) }
}

/// An isotope typing context
#[derive(Debug, Clone)]
pub struct Context<M, Sym=IStr, Mem=IStr> {
    /// The values in this context
    vals: ValArena<Sym, Mem>,
    /// The metadata in this context
    metadata: HashMap<DefId, M>
}

impl<M> Context<M> {
    /// Create a new context with the given capacities
    pub fn with_capacity(values: usize, metadata: usize) -> Self {
        let mut res = Context {
            vals: ValArena::with_capacity(values),
            metadata: HashMap::with_capacity(metadata)
        };
        // Insert the root element
        res.define(Tuple::nil().into());
        res
    }
    /// Create a new context with no capacity
    pub fn new() -> Self { Self::with_capacity(0, 0) }
    /// Define a new value in a context, returning it's assigned value ID
    /// Panics if the definition contains any invalid value IDs
    pub fn define(&mut self, def: Definition) -> ValId {
        let new_val = Value {
            ty: SmallVec::new(),
            eq: ValId::invalid(),
            flags: def.kind().flags(),
            def
        };
        let id = ValId(self.vals.insert(new_val));
        // Set equality group to a self-loop for intrusive disjoint set forest
        self.vals[id].eq = id;
        id
    }
    /// Get the root element's ID in this context
    #[inline] pub fn root_id() -> DefId { DefId::default() }
    /// Borrow the root element of this context
    #[inline] pub fn root(&self) -> &Value { &self.vals[Self::root_id()] }
    /// Mutably borrow the root element of this context
    #[inline] pub fn root_mut(&mut self) -> &mut Value { &mut self.vals[Self::root_id()] }
}

impl<M> Index<DefId> for Context<M> {
    type Output = Value;
    #[inline] fn index(&self, v: DefId) -> &Value { self.vals.index(v) }
}

impl<M> Index<ValId> for Context<M> {
    type Output = Value;
    #[inline] fn index(&self, v: ValId) -> &Value { self.vals.index(v) }
}

impl<M> Index<TypeId> for Context<M> {
    type Output = Value;
    #[inline] fn index(&self, v: TypeId) -> &Value { self.vals.index(v) }
}

impl<M> IndexMut<DefId> for Context<M> {
    #[inline] fn index_mut(&mut self, v: DefId) -> &mut Value { self.vals.index_mut(v) }
}

impl<M> IndexMut<ValId> for Context<M> {
    #[inline] fn index_mut(&mut self, v: ValId) -> &mut Value { self.vals.index_mut(v) }
}

impl<M> IndexMut<TypeId> for Context<M> {
    #[inline] fn index_mut(&mut self, v: TypeId) -> &mut Value { self.vals.index_mut(v) }
}

/// Flags applying to an isotope value
#[derive(Debug, EnumSetType)]
pub enum ValueFlag {
    /// Whether the list of types added may be out of order. If 0, this means it is sorted
    TypesUnsorted,
    /// Whether this value is guaranteed to be a type
    IsType,
    /// Whether this value is guaranteed to be a kind, i.e. a type of types
    IsKind
}

pub type ValueFlags = EnumSet<ValueFlag>;

/// An isotope value
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Value<Sym=IStr, Mem=IStr> {
    /// The definition of this value
    def : Definition<Sym, Mem>,
    /// The type judgements applying to this value. Use the first one that fits.
    ty : SmallVec<[TypeId; 1]>,
    /// The equality group this value is part of
    eq : ValId,
    /// Flags applying to this value
    flags: ValueFlags
}

impl Value {
    /// Get an iterator over the values referenced by this value
    #[inline] pub fn referenced(&mut self) -> impl '_ + Iterator<Item=ValId> {
        let def_iter = self.def.dependencies();
        let ty_iter = self.ty.iter().map(|t| (*t).into());
        let eq_iter = Some(self.eq).into_iter();
        def_iter.chain(ty_iter).chain(eq_iter)
    }
    /// Whether this value is currently marked as a type
    //TODO: think about an is_type marker. Or an is_kind one?
    #[inline] pub fn is_type(&self) -> bool {
        self.flags.contains(ValueFlag::IsType)
    }
    /// Get the definition of this value
    #[inline] pub fn def(&self) -> &Definition { &self.def }
    /// Get the type judgements applying to this value
    #[inline] pub fn ty(&self) -> &[TypeId] { self.ty.as_slice() }
    /// Get the equality group of this value
    #[inline] pub fn eq(&self) -> ValId { self.eq }
    /// Get the flags of this value
    #[inline] pub fn flags(&self) -> ValueFlags { self.flags }
    /// Check whether this value *might* have been parametrized by a given
    /// node's parameters
    //TODO: improve
    #[inline] pub fn maybe_parametrized(&self, _node: ValId) -> bool { true }
}

/// Kinds of isotope value definition
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum DefKind {
    Expr,
    Proj,
    Member,
    Tuple,
    Param,
    Lambda,
    Pi,
    Sigma,
    Symbol,
    Universe,
    Ind,
    Refl,
    Id,
    Inductive
}

impl DefKind {
    /// Get whether this kind is guaranteed always to be a type
    #[inline]
    pub fn is_type(&self) -> bool {
        use DefKind::*;
        match self { Pi | Sigma => true, _ => false }
    }
    /// Get whether this kind is guaranteed always to be a kind
    #[inline]
    pub fn is_kind(&self) -> bool {
        //use DefKind::*;
        // Waiting for universe nodes
        match self { _ => false }
    }
    /// Get the flags corresponding to this definition kind
    #[inline]
    pub fn flags(&self) -> ValueFlags {
        let mut res = ValueFlags::empty();
        if self.is_type() { res |= ValueFlag::IsType }
        if self.is_kind() { res |= ValueFlag::IsKind }
        res
    }
    /// Get the flags are correlated with definition kinds
    #[inline]
    pub fn def_kind_flags() -> ValueFlags {
        ValueFlag::IsKind | ValueFlag::IsType
    }
}

/// The definition of an isotope value
#[derive(Debug, Clone, Eq, PartialEq)]
#[derive(From)]
pub enum Definition<Sym=IStr, Mem=IStr> {
    Expr(Expr),
    Proj(Proj),
    Member(Member<Mem>),
    Tuple(Tuple),
    Param(Param),
    Lambda(Lambda),
    Pi(Pi),
    Sigma(Sigma),
    Symbol(Symbol<Sym>),
    Universe(Universe),
    Ind(Ind),
    Refl(Refl),
    Id(Id),
    Inductive(Inductive)
}

impl Definition {
    /// Get an iterator over the values this definition depends on
    #[inline] pub fn dependencies<'a>(&'a self) -> impl 'a + Iterator<Item=ValId> {
        match self {
            Definition::Expr(Expr{args: v}) | Definition::Tuple(Tuple{elements: v}) => {
                None.into_iter().chain(v.iter().copied())
            },
            Definition::Lambda(Lambda{result, params})
            | Definition::Pi(Pi{result, params})
            | Definition::Sigma(Sigma{result, params}) => {
                Some(*result).into_iter().chain(params.iter().copied())
            },
            Definition::Proj(Proj{src,..}) | Definition::Member(Member{src,..}) |
            Definition::Refl(Refl(src)) => {
                Some(*src).into_iter().chain([].iter().copied())
            },
            Definition::Ind(Ind(ty)) | Definition::Id(Id(ty)) => {
                Some((*ty).into()).into_iter().chain([].iter().copied())
            },
            Definition::Param(_) | Definition::Symbol(_) | Definition::Universe(_) => {
                None.into_iter().chain([].iter().copied())
            },
            Definition::Inductive(Inductive{ constructors }) => {
                None.into_iter().chain(constructors.iter().copied())
            }
        }
    }
    /// Get a mutable iterator over the values this definition depends on
    #[inline] pub fn dependencies_mut<'a>(&'a mut self) -> impl 'a + Iterator<Item=&mut ValId> {
        match self {
            Definition::Expr(Expr{args: v}) | Definition::Tuple(Tuple{elements: v}) => {
                None.into_iter().chain(v.iter_mut())
            },
            Definition::Lambda(Lambda{result, params})
            | Definition::Pi(Pi{result, params})
            | Definition::Sigma(Sigma{result, params}) => {
                Some(result).into_iter().chain(params.iter_mut())
            },
            Definition::Proj(Proj{src,..}) | Definition::Member(Member{src,..}) |
            Definition::Refl(Refl(src)) => {
                Some(src).into_iter().chain([].iter_mut())
            },
            Definition::Ind(Ind(ty)) | Definition::Id(Id(ty)) => {
                Some(ty).into_iter().chain([].iter_mut())
            },
            Definition::Param(_) | Definition::Symbol(_) | Definition::Universe(_) => {
                None.into_iter().chain([].iter_mut())
            },
            Definition::Inductive(Inductive{ constructors }) => {
                None.into_iter().chain(constructors.iter_mut())
            }
        }
    }
    /// Get the kind of this definition
    #[inline] pub fn kind(&self) -> DefKind {
        match self {
            Definition::Expr(_) => DefKind::Expr,
            Definition::Proj(_) => DefKind::Proj,
            Definition::Member(_) => DefKind::Member,
            Definition::Tuple(_) => DefKind::Tuple,
            Definition::Param(_) => DefKind::Param,
            Definition::Lambda(_) => DefKind::Lambda,
            Definition::Pi(_) => DefKind::Pi,
            Definition::Sigma(_) => DefKind::Sigma,
            Definition::Symbol(_) => DefKind::Symbol,
            Definition::Universe(_) => DefKind::Universe,
            Definition::Ind(_) => DefKind::Ind,
            Definition::Refl(_) => DefKind::Refl,
            Definition::Id(_) => DefKind::Id,
            Definition::Inductive(_) => DefKind::Inductive
        }
    }
}

/// A tuple of isotope expressions
#[derive(Debug, Clone, Eq, PartialEq)]
#[derive(From, Into, Index, IndexMut)]
pub struct Tuple {
    /// The elements of this tuple
    pub elements: Vec<ValId>
}

impl Tuple {
    /// Get the empty tuple
    #[inline] pub fn nil() -> Self { Tuple { elements : Vec::new() } }
}

/// An isotope expression
#[derive(Debug, Clone, Eq, PartialEq)]
#[derive(From, Into, Index, IndexMut)]
pub struct Expr {
    /// The arguments to this S-expression
    pub args: Vec<ValId>
}

/// A projection from an isotope value
#[derive(Debug, Clone, Eq, PartialEq)]
#[derive(From, Into)]
pub struct Proj {
    /// The source of this projection
    pub src: ValId,
    /// The index projected to
    pub proj: TupleIx
}

/// A projection of member of an isotope expression.
#[derive(Debug, Clone, Eq, PartialEq)]
#[derive(From)]
pub struct Member<Mem=IStr> {
    /// The source of this projection
    pub src: ValId,
    /// The member projected to
    pub mem: Mem
}

/// A parameter to an isotope function or module
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Param;

/// An isotope lambda expression
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Lambda {
    /// Ordered parameters
    pub params: Vec<ValId>,
    /// Result
    pub result: ValId
}

/// An isotope pi type
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Pi {
    /// Ordered parameters
    pub params: Vec<ValId>,
    /// Result
    pub result: ValId
}

/// An isotope sum type
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Sigma {
    /// Ordered parameters
    pub params: Vec<ValId>,
    /// Result
    pub result: ValId
}

/// A symbol which should be linked
//TODO: consider merging with `Param` somehow...
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Symbol<Sym=IStr>(Sym);

/// A typing universe
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Universe;

/// Get the inductor of the type passed in
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Ind(pub ValId);

/// Get the reflexivity element of the value passed in (i.e. for `x`, get the proof `x = x`)
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Refl(pub ValId);

/// Get the identity type of the type passed in
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Id(pub ValId);

/// An inductive type
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Inductive {
    /// Constructor types
    pub constructors: Vec<ValId>
}
