/*!
Utilities to display a value in an isotope typin;g cext
*/
use std::fmt::{self, Display, Formatter};
use super::{
    DefId, ValId, TypeId,
    Value,
    Definition, Expr, Tuple, Proj, Member, Param, Lambda, Pi, Sigma, Universe, Ind, Refl, Id,
    Inductive
};

type FR = Result<(), fmt::Error>;

impl Display for DefId {
    fn fmt(&self, fmt: &mut Formatter) -> FR {
        write!(fmt, "%{}", self.0.to_idx())
    }
}

impl Display for ValId {
    fn fmt(&self, fmt: &mut Formatter) -> FR {
        write!(fmt, "%{}", self.0.to_idx())
    }
}

impl Display for TypeId {
    fn fmt(&self, fmt: &mut Formatter) -> FR {
        write!(fmt, "%{}", self.0.to_idx())
    }
}

fn display_separated<F, I>(items: I, c: &mut F, fmt: &mut Formatter) -> FR
    where I: Iterator, F: FnMut(I::Item, &mut Formatter) -> FR {
    let mut first = true;
    for item in items {
        if first { first = false } else { write!(fmt, " ")? }
        c(item, fmt)?;
    }
    Ok(())
}

impl Expr {
    pub fn fmt_cc<F>(&self, c: &mut F, fmt: &mut Formatter) -> FR
    where F: FnMut(ValId, &mut Formatter) -> FR {
        write!(fmt, "(")?;
        display_separated(self.args.iter().copied(), c, fmt)?;
        write!(fmt, ")")
    }
}

impl Tuple {
    pub fn fmt_cc<F>(&self, c: &mut F, fmt: &mut Formatter) -> FR
    where F: FnMut(ValId, &mut Formatter) -> FR {
        write!(fmt, "[")?;
        display_separated(self.elements.iter().copied(), c, fmt)?;
        write!(fmt, "]")
    }
}

impl Proj {
    pub fn fmt_cc<F>(&self, c: &mut F, fmt: &mut Formatter) -> FR
    where F: FnMut(ValId, &mut Formatter) -> FR {
        write!(fmt, "#proj {}", self.proj)?;
        c(self.src, fmt)
    }
}

impl Member {
    pub fn fmt_cc<F>(&self, c: &mut F, fmt: &mut Formatter) -> FR
    where F: FnMut(ValId, &mut Formatter) -> FR {
        write!(fmt, "#get {}", self.mem)?;
        c(self.src, fmt)
    }
}

impl Display for Param {
    fn fmt(&self, fmt: &mut Formatter) -> FR {
        write!(fmt, "#param")
    }
}

impl Display for Universe {
    fn fmt(&self, fmt: &mut Formatter) -> FR {
        write!(fmt, "#universe")
    }
}

impl Lambda {
    pub fn fmt_cc<F>(&self, c: &mut F, fmt: &mut Formatter) -> FR
    where F: FnMut(ValId, &mut Formatter) -> FR {
        write!(fmt, "#lambda {{")?;
        display_separated(self.params.iter().copied(), c, fmt)?;
        write!(fmt, "}} {}", self.result)
    }
}

impl Pi {
    pub fn fmt_cc<F>(&self, c: &mut F, fmt: &mut Formatter) -> FR
    where F: FnMut(ValId, &mut Formatter) -> FR {
        write!(fmt, "#pi {{")?;
        display_separated(self.params.iter().copied(), c, fmt)?;
        write!(fmt, "}}")?;
        c(self.result, fmt)
    }
}

impl Sigma {
    pub fn fmt_cc<F>(&self, c: &mut F, fmt: &mut Formatter) -> FR
    where F: FnMut(ValId, &mut Formatter) -> FR {
        write!(fmt, "#sigma {{")?;
        display_separated(self.params.iter().copied(), c, fmt)?;
        write!(fmt, "}} ")?;
        c(self.result, fmt)
    }
}

impl Ind {
    pub fn fmt_cc<F>(&self, c: &mut F, fmt: &mut Formatter) -> FR
    where F: FnMut(ValId, &mut Formatter) -> FR {
        write!(fmt, "#ind ")?;
        c(self.0.into(), fmt)
    }
}

impl Refl {
    pub fn fmt_cc<F>(&self, c: &mut F, fmt: &mut Formatter) -> FR
    where F: FnMut(ValId, &mut Formatter) -> FR {
        write!(fmt, "#refl ")?;
        c(self.0, fmt)
    }
}

impl Id {
    pub fn fmt_cc<F>(&self, c: &mut F, fmt: &mut Formatter) -> FR
    where F: FnMut(ValId, &mut Formatter) -> FR {
        write!(fmt, "#id ")?;
        c(self.0.into(), fmt)
    }
}

impl Inductive {
    pub fn fmt_cc<F>(&self, c: &mut F, fmt: &mut Formatter) -> FR
    where F: FnMut(ValId, &mut Formatter) -> FR {
        write!(fmt, "#inductive[")?;
        display_separated(self.constructors.iter().map(|ty| (*ty).into()), c, fmt)?;
        write!(fmt, "]")
    }
}

impl Definition {
    pub fn fmt_cc<F>(&self, c: &mut F, fmt: &mut Formatter) -> FR
    where F: FnMut(ValId, &mut Formatter) -> FR {
        use Definition::*;
        match self {
            Expr(e) => e.fmt_cc(c, fmt),
            Tuple(t) => t.fmt_cc(c, fmt),
            Proj(p) => p.fmt_cc(c, fmt),
            Param(p) => p.fmt(fmt),
            Lambda(l) => l.fmt_cc(c, fmt),
            Pi(p) => p.fmt_cc(c, fmt),
            Sigma(s) => s.fmt_cc(c, fmt),
            Member(m) => m.fmt_cc(c, fmt),
            Symbol(s) => s.0.fmt(fmt),
            Universe(u) => u.fmt(fmt),
            Ind(i) => i.fmt_cc(c, fmt),
            Refl(r) => r.fmt_cc(c, fmt),
            Id(i) => i.fmt_cc(c, fmt),
            Inductive(i) => i.fmt_cc(c, fmt)
        }
    }
}

impl Value {
    pub fn fmt_cc<F>(&self, c: &mut F, fmt: &mut Formatter) -> FR
    where F: FnMut(ValId, &mut Formatter) -> FR {
        write!(fmt, "#define {} ", self.eq)?;
        for _ in 0..self.ty.len() { write!(fmt, "(")? }
        self.def.fmt_cc(c, fmt)?;
        for ty in self.ty.iter() {
            write!(fmt, " : {})", ty)?;
        }
        Ok(())
    }
}

impl Display for Value {
    fn fmt(&self, fmt: &mut Formatter) -> FR {
        self.fmt_cc(&mut |val: ValId, fmt| val.fmt(fmt), fmt)
    }
}
