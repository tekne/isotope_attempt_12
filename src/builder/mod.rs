/*!
Builder for an isotope typing context
*/
use std::convert::TryInto;

use crate::context::{
    Context, DefId, ValId, Tuple
};

pub mod sexpr;
pub mod tuple;
pub mod proj;
pub mod param;
pub mod universe;
pub mod id;
pub mod ind;
pub mod refl;
pub mod lambda;

/// A builder for an isotope typing context
#[derive(Debug)]
pub struct ContextBuilder<'a, M> {
    /// The underlying typing context
    ctx: &'a mut Context<M>,
    /// The cached nil. root => not yet generated
    cached_nil: DefId
}

impl<'a, M> ContextBuilder<'a, M> {
    /// Create a new `ContextBuilder`, borrowing the given `Context`
    pub fn new(ctx: &'a mut Context<M>) -> Self {
        Self {
            cached_nil: DefId::default(),
            ctx
        }
    }
    /// Immutably borrow the `Context` underlying this `ContextBuilder`
    pub fn ctx(&self) -> &Context<M> { self.ctx }
    /// Mutably borrow the `Context` underlying this `ContextBuilder`
    pub fn ctx_mut(&mut self) -> &mut Context<M> { self.ctx }
    /// Get this builder's nil, if it has been cached
    pub fn nil(&self) -> Option<ValId> { self.cached_nil.try_into().ok() }
    /// Get this builder's nil if it has been cached, or create one otherwise
    pub fn get_nil(&mut self) -> ValId {
        if let Some(nil) = self.nil() { nil } else {
            let res = self.ctx.define(Tuple::nil().into());
            self.cached_nil = res.into();
            res
        }
    }
}
