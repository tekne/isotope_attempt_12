use crate::context::{ValId, Ind};
use super::ContextBuilder;

impl<'a, M> ContextBuilder<'a, M> {
    pub fn build_ind(&mut self, v: ValId) -> ValId {
        self.ctx.define(Ind(v).into())
    }
}
