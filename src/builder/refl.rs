use crate::context::{TypeId, ValId, Refl};
use super::ContextBuilder;

impl<'a, M> ContextBuilder<'a, M> {
    pub fn build_refl(&mut self, v: ValId) -> TypeId {
        TypeId::assert_type(self.ctx.define(Refl(v).into()))
    }
}
