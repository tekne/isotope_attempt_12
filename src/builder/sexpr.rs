/*!
Utilities for building S-expressions, and associated types
*/
use hashbrown::hash_map::HashMap;
use std::iter::FromIterator;

use crate::context::{
    Definition,
    Expr,
    Lambda,
    ValId,
    substitution::SubstitutionError
};
use super::{
    ContextBuilder
};

/// A type error which can occur during evaluation
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum EvalTypeError {
    /// Cannot evaluate a type
    EvalType(ValId)
}

impl From<SubstitutionError> for EvalTypeError {
    fn from(err: SubstitutionError) -> EvalTypeError { match err {} }
}

impl<'a, M> ContextBuilder<'a, M> {
    /// Build a normalized S-expression with the given arguments.
    /// Guarantees one-step (or more) normalization.
    fn normalize_sexpr<V>(&mut self, args: V) -> Result<ValId, EvalTypeError> where
    V: AsRef<[ValId]> + Into<Vec<ValId>> {
        use EvalTypeError::*;
        let vargs: &[ValId] = args.as_ref();
        let f = vargs[0];
        if self.ctx[f].is_type() {
            return Err(EvalType(f))
        }
        match self.ctx[f].def() {
            Definition::Pi(_) | Definition::Sigma(_) => {return Err(EvalType(f))},
            // Parameters, symbols and members are always opaque
            Definition::Param(_) | Definition::Symbol(_) | Definition::Member(_) =>
                Ok(self.ctx.define(Expr::from(args.into()).into())),
            // Lambda functions are evalated by substitution
            //TODO: think about efficiency for the compiled case
            //(as this amounts to always inlining)
            Definition::Lambda(Lambda { params, result }) => {
                let no_params = params.len();
                if no_params <= vargs.len() { // Currying
                    let mut subs = HashMap::from_iter(
                        params.iter().copied().zip(vargs.iter().copied())
                    );
                    let result = *result;
                    let partial_sub = self.ctx.substitute(&mut subs, result, result.into())
                        .map_err(|err| EvalTypeError::from(err))?;
                    let diff = vargs.len() - no_params;
                    if diff > 0 {
                        let mut new_args: Vec<ValId> = Vec::with_capacity(diff + 1);
                        new_args.push(partial_sub);
                        new_args.extend(&vargs[no_params..]);
                        self.normalize_sexpr(new_args)
                    } else {
                        Ok(partial_sub)
                    }
                } else { // Closure
                    unimplemented!()
                }
            },
            _ => unimplemented!()
        }
    }
    /// Build an S-expression. Return a type error if one is detected.
    pub fn build_sexpr<V>(&mut self, args: V) -> Result<ValId, EvalTypeError> where
    V: AsRef<[ValId]> + Into<Vec<ValId>> {
        let vargs: &[ValId] = args.as_ref();
        match vargs.len() {
            0 => Ok(self.get_nil()),
            1 => Ok(vargs[0]),
            _ => self.normalize_sexpr(args)
        }
    }
}
