use crate::context::{
    ValId,
    Lambda
};

use super::ContextBuilder;

impl <'a, M> ContextBuilder<'a, M> {
    pub fn build_lambda<V>(&mut self, params: V, result: ValId) -> ValId where
    V: AsRef<[ValId]> + Into<Vec<ValId>> {
        self.ctx.define(
            Lambda {
                params: params.into(),
                result
            }.into()
        )
    }
}
