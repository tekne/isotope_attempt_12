use crate::context::{ValId, Universe};
use super::ContextBuilder;

impl<'a, M> ContextBuilder<'a, M> {
    #[inline] pub fn build_universe(&mut self) -> ValId { self.ctx.define(Universe.into()) }
}
