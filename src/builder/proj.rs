/*!
Utilities for building projections, and associated types
*/
use std::convert::TryFrom;
use crate::TupleIx;
use crate::context::{
    Proj, ValId, Definition
};
use super::{
    ContextBuilder
};

/// A type error detected which can occur when building a projection
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum BuildProjError {
    /// Attempted to project too high an index
    IndexOutOfBounds { maximum: TupleIx, proj: TupleIx }
}

impl<'a, M> ContextBuilder<'a, M> {
    /// Build a projection with one-step (or more) normalization.
    pub fn build_proj(&mut self, src: ValId, proj: TupleIx) -> Result<ValId, BuildProjError> {
        match self.ctx[src].def() {
            Definition::Tuple(t) => {
                t.elements.get(usize::try_from(proj).expect("Index to usize overflow"))
                    .copied()
                    .ok_or(BuildProjError::IndexOutOfBounds {
                        maximum: t.elements.len() as TupleIx,
                        proj
                    })
            },
            //TODO: think about sigma types, et al.!
            _ => Ok(self.ctx.define(Proj{src, proj}.into()))
        }
    }
}
