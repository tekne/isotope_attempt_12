use crate::context::{ValId, Param};
use super::ContextBuilder;

impl<'a, M> ContextBuilder<'a, M> {
    #[inline] pub fn build_param(&mut self) -> ValId { self.ctx.define(Param.into()) }
}
