/*!
Utilities for building tuples, and associated types
*/

use crate::context::{
    Tuple,
    ValId
};
use super::{
    ContextBuilder
};

/// A type error which can occur during evaluation
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum TupleTypeError {

}

impl<'a, M> ContextBuilder<'a, M> {
    /// Build a tuple. Return a type error if one is detected.
    pub fn build_tuple<V>(&mut self, args: V) -> Result<ValId, TupleTypeError> where
        V: AsRef<[ValId]> + Into<Vec<ValId>> {
        let vargs: &[ValId] = args.as_ref();
        match vargs.len() {
            0 => Ok(self.get_nil()),
            1 => Ok(vargs[0]),
            _ => Ok(self.ctx.define(Tuple{ elements: args.into() }.into()))
        }
    }
}
