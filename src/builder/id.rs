use crate::context::{ValId, TypeId, Id};
use super::ContextBuilder;

impl<'a, M> ContextBuilder<'a, M> {
    pub fn build_id(&mut self, v: ValId) -> TypeId {
        TypeId::assert_type(self.ctx.define(Id(v).into()))
    }
}
