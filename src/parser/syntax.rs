/*!
A parser implementation for isotope expressions
*/
use internship::IStr;
use nom::{
    IResult,
    branch::alt,
    combinator::{map, map_res, map_parser, all_consuming, opt, self},
    sequence::{tuple, delimited, preceded, terminated},
    multi::many0,
    bytes::complete::{tag, is_not, is_a},
    character::complete::{multispace0, multispace1, digit1, hex_digit1},
};
use num::BigUint;
use std::iter::FromIterator;
use bitvec::vec::BitVec;

use crate::TupleIx;
use super::ast::{
    Expr, Value, List, Dictionary, Parameters, TypedSymbol,
    Lambda, Pi, Sigma, Inductive, Struct, Finite, ByteVector, Record, BitVector, Universe,
    Get, Projection, Id, Refl, Inductor, Define,
    DictionaryMap, BitVectorInt, FiniteInt
};

/// The characters which cannot appear in an isotope symbol
pub const SPECIAL_CHARS : &str = " \t\r\n(){}[]#:";

pub fn parse_expr(input: &str) -> IResult<&str, Expr> {
    let (rest, mut value) = parse_value(input)?;
    let (rest, judgement) = opt(map(parse_type_judgement, |ty| Box::new(ty)))(rest)
        .expect("Cannot fail");
    value.ty = judgement.or(value.ty);
    Ok((rest, value))
}

pub fn parse_value(input: &str) -> IResult<&str, Expr> {
    delimited(multispace0, alt((
        parse_special,
        parse_sexpr,
        parse_tuple,
        parse_record,
        parse_symbol
    )), multispace0)(input)
}

pub fn parse_special(input: &str) -> IResult<&str, Expr> {
    preceded(
        tag("#"),
        alt((
            map(parse_lambda, |lam| lam.into()),
            map(parse_pi, |pi| pi.into()),
            map(parse_sigma, |sigma| sigma.into()),
            map(parse_inductive, |ind| ind.into()),
            map(parse_named_record, |rec| rec.into()),
            map(parse_struct, |st| st.into()),
            map(parse_finite, |fin| fin.into()),
            map(parse_byte_vector, |by| by.into()),
            map(parse_universe, |u| u.into()),
            map(parse_identity, |i| i.into()),
            map(parse_refl, |r| r.into()),
            map(parse_inductor, |i| i.into()),
            map(parse_define, |d| d.into()),
            parse_projection,
            parse_get
        ))
    )(input)
}

pub fn parse_tuple_ix(input: &str) -> IResult<&str, TupleIx> {
    map_res(digit1, |digits: &str| digits.parse::<TupleIx>())(input)
}

pub fn parse_projection(input: &str) -> IResult<&str, Expr> {
    preceded(
        delimited(multispace0, tag("proj"), multispace0),
        map_res(
            tuple((parse_tuple_ix, parse_expr)),
            |(ix, source)| {
                match source.value {
                    Value::Tuple(mut l) => {
                        let ix = ix as usize;
                        if ix > l.0.elements.len() {
                            Err(())
                        } else {
                            Ok(l.0.elements.swap_remove(ix))
                        }

                    },
                    _ => Ok(Projection { ix, source : source.into() }.into())
                }
            }
        )
    )(input)
}

pub fn parse_get(input: &str) -> IResult<&str, Expr> {
    preceded(
        delimited(multispace0, tag("get"), multispace1),
        map_res(
            tuple((
                delimited(multispace0, map(parse_ident, |ident| IStr::new(ident)), multispace0),
                parse_expr
            )),
            |(field, source)| {
                match source.value {
                    Value::Record(Record{mut members,..}) | Value::Struct(Struct{mut members,..})
                        => members.map.remove(&field).ok_or(()),
                    _   => Ok(Get { field, source : source.into() }.into())
                }
            }
        )
    )(input)
}

pub fn parse_multi_params(input: &str) -> IResult<&str, Parameters> {
    map(many0(parse_typed_symbol), |symbols| Parameters { symbols })(input)
}

pub fn parse_params(input: &str) -> IResult<&str, Parameters> {
    alt((
        preceded(multispace0, delimited(tag("{"), parse_multi_params, tag("}"))),
        preceded(multispace1, map(parse_typed_symbol, |ts| Parameters { symbols : vec![ts] }))
    ))(input)
}

pub fn parse_lambda(input: &str) -> IResult<&str, Lambda> {
    preceded(
        preceded(multispace0, tag("lambda")),
        map(
            tuple((parse_params, parse_expr)),
            |(params, result)| Lambda { params, result : Box::new(result) }
        )
    )(input)
}

pub fn parse_pi(input: &str) -> IResult<&str, Pi> {
    preceded(
        preceded(multispace0, tag("pi")),
        map(
            tuple((parse_params, parse_expr)),
            |(params, result)| Pi { params, ty : Box::new(result) }
        )
    )(input)
}

pub fn parse_sigma(input: &str) -> IResult<&str, Sigma> {
    preceded(
        preceded(multispace0, tag("sigma")),
        map(
            tuple((parse_params, parse_expr)),
            |(params, result)| Sigma { params, ty : Box::new(result) }
        )
    )(input)
}

pub fn parse_inductive(input: &str) -> IResult<&str, Inductive> {
    map(
        tuple((
            preceded(multispace0, alt((
                map(tag("inductive"), |_| false),
                map(tag("data"), |_| true)
            ))),
            parse_opt_name,
            parse_bracketed_dict
        )),
        |(is_data, name, constructors)| Inductive { is_data, name, constructors }
    )(input)
}

pub fn parse_opt_name(input: &str) -> IResult<&str, Option<IStr>> {
    map(
        opt(delimited(multispace1, parse_ident, multispace0)),
        |opt_name| opt_name.map(|name| IStr::new(name))
    )(input)
}

pub fn parse_bracketed_dict(input: &str) -> IResult<&str, Dictionary> {
    delimited(
        delimited(multispace0, tag("{"), multispace0),
        parse_dictionary,
        delimited(multispace0, tag("}"), multispace0)
    )(input)
}

pub fn parse_named_record(input: &str) -> IResult<&str, Record> {
    preceded(
        preceded(multispace0, tag("record")),
        map(
            tuple((parse_opt_name, parse_bracketed_dict)),
            |(name, members)| Record { name, members }
        )
    )(input)
}

pub fn parse_struct(input: &str) -> IResult<&str, Struct> {
    preceded(
        preceded(multispace0, tag("struct")),
        map(
            tuple((parse_opt_name, parse_bracketed_dict)),
            |(name, members)| Struct { name, members }
        )
    )(input)
}

pub fn parse_finite(input: &str) -> IResult<&str, Finite> {
    preceded(
        preceded(multispace0, tag("finite")),
        map(
            tuple((parse_opt_name, parse_finite_integer)),
            |(name, values)| Finite { name, values }
        )
    )(input)
}

pub fn parse_byte_vector(input: &str) -> IResult<&str, ByteVector> {
    preceded(
        delimited(multispace0, tag("bytes"), multispace0),
        delimited(
            delimited(multispace0, tag("["), multispace0),
            parse_inner_byte_vector,
            delimited(multispace0, tag("]"), multispace0)
        )
    )(input)
}

pub fn parse_inner_byte_vector(_input: &str) -> IResult<&str, ByteVector> {
    unimplemented!()
}

pub fn parse_universe(input: &str) -> IResult<&str, Universe> {
    map(delimited(multispace0, tag("universe"), multispace0), |_| Universe)(input)
}

pub fn parse_sexpr(input: &str) -> IResult<&str, Expr> {
    delimited(
        tag("("),
        map(parse_list, |mut lst| {
            if lst.elements.len() == 1 {
                lst.elements.remove(0)
            } else {
                Value::Sexpr(lst.into()).into()
            }
        }),
        tag(")")
    )(input)
}

pub fn parse_tuple(input: &str) -> IResult<&str, Expr> {
    delimited(
        tag("["),
        map(parse_list, |mut lst| {
            if lst.elements.len() == 1 {
                lst.elements.remove(0)
            } else {
                Value::Tuple(lst.into()).into()
            }
        }),
        tag("]")
    )(input)
}

pub fn parse_list(input: &str) -> IResult<&str, List> {
    map(many0(parse_expr), |elements| List { elements })(input)
}

pub fn parse_record(input: &str) -> IResult<&str, Expr> {
    delimited(tag("{"),
        delimited(
            multispace0,
            map(
                parse_dictionary,
                |members| Record{ name : None, members }.into()
            ),
            multispace0
        ),
        tag("}")
    )(input)
}

pub fn parse_named_expr(input: &str) -> IResult<&str, (IStr, Expr)> {
    map(
        tuple((parse_ident, parse_colon, parse_expr)),
        |(ident, _, expr)| (IStr::new(ident), expr)
    )(input)
}

pub fn parse_dictionary(input: &str) -> IResult<&str, Dictionary> {
    map(
        many0(parse_named_expr),
        |exprs| Dictionary { map : DictionaryMap::from_iter(exprs.into_iter()) }
    )(input)
}

pub fn parse_colon(input: &str) -> IResult<&str, &str> {
    delimited(multispace0, tag(":"), multispace0)(input)
}

pub fn parse_bitvector_integer(input: &str) -> IResult<&str, BitVectorInt> {
    map_res(digit1, |s: &str| s.parse::<BitVectorInt>())(input)
}

pub fn parse_finite_integer(input: &str) -> IResult<&str, FiniteInt> {
    map_res(digit1, |s: &str| s.parse::<FiniteInt>())(input)
}

pub fn parse_bits(input: &str) -> IResult<&str, Expr> {
    map_res(
        tuple((
            opt(parse_bitvector_integer),
            tag("'"),
            alt((
                parse_binary,
                parse_decimal_bits,
                parse_octal_bits,
                parse_hexadecimal_bits
            ))
        )),
        |(s, _, mut b)| {
            if let Some(size) = s {
                // Type inference acting strangely...
                if let Err(_) = b.resize(size as usize) {
                    return Err(())
                }
            }
            Ok(b.into())
        }
    )(input)
}

pub fn parse_binary(input: &str) -> IResult<&str, BitVector> {
    map(
        is_a("01"),
        |bits: &str| {
            let mut res = BitVec::new();
            for bit in bits.bytes() {
                res.push(bit == b'1')
            }
            BitVector(res)
        }
    )(input)
}

pub fn parse_decimal_bits(_input: &str) -> IResult<&str, BitVector> {
    unimplemented!()
}

pub fn parse_octal_bits(_input: &str) -> IResult<&str, BitVector> {
    unimplemented!()
}

pub fn parse_hexadecimal_bits(_input: &str) -> IResult<&str, BitVector> {
    unimplemented!()
}

pub fn parse_number(input: &str) -> IResult<&str, Expr> {
    alt((
        preceded(tag("0x"), parse_hexadecimal),
        parse_decimal
    ))(input)
}

pub fn radix_to_expr(radix: u32, digits: &str) -> Result<Expr, ()> {
    FiniteInt::from_str_radix(digits, radix)
        .ok()
        .map(|n| Value::SmallNatural(n).into())
        .or(
            BigUint::parse_bytes(digits.as_bytes(), radix)
            .map(|n| Value::Natural(n).into())
        )
        .ok_or(())
}

pub fn parse_decimal(input: &str) -> IResult<&str, Expr> {
    map_res(
        digit1,
        |digits: &str| radix_to_expr(10, digits)
    )(input)
}

pub fn parse_hexadecimal(input: &str) -> IResult<&str, Expr> {
    map_res(
        hex_digit1,
        |digits: &str| radix_to_expr(16, digits)
    )(input)
}

pub fn parse_symbol(input: &str) -> IResult<&str, Expr> {
    map_parser(
        parse_ident,
        alt((
            all_consuming(alt((
                parse_bits,
                parse_number
            ))),
            map(combinator::rest, |ident: &str| Value::Symbol(ident.into()).into())
        ))
    )(input)
}

pub fn parse_typed_symbol(input: &str) -> IResult<&str, TypedSymbol> {
    map(
        tuple((parse_ident, opt(parse_type_judgement))),
        |(ident, ty)| TypedSymbol { name : ident.into(), ty: ty.map(|ty| Box::new(ty)) }
    )(input)
}

pub fn parse_ident(input: &str) -> IResult<&str, &str> {
    is_not(SPECIAL_CHARS)(input)
}

pub fn parse_type_judgement(input: &str) -> IResult<&str, Expr> {
    preceded(
        delimited(multispace0, tag(":"), multispace0),
        parse_expr
    )(input)
}

pub fn parse_identity(input: &str) -> IResult<&str, Id> {
    preceded(
        delimited(multispace0, tag("id"), multispace1),
        map(
            parse_expr,
            |expr| Id(expr.into())
        )
    )(input)
}

pub fn parse_refl(input: &str) -> IResult<&str, Refl> {
    preceded(
        delimited(multispace0, tag("refl"), multispace1),
        map(
            parse_expr,
            |expr| Refl(expr.into())
        )
    )(input)
}

pub fn parse_inductor(input: &str) -> IResult<&str, Inductor> {
    preceded(
        delimited(multispace0, tag("ind"), multispace1),
        map(
            parse_expr,
            |expr| Inductor(expr.into())
        )
    )(input)
}

pub fn parse_define(input: &str) -> IResult<&str, Define> {
    preceded(
        delimited(multispace0, tag("define"), multispace1),
        map(
            tuple((
                terminated(parse_ident, multispace1),
                parse_expr
            )),
            |(name, val)| Define { name: name.into(), val: val.into() }
        )
    )(input)
}

#[cfg(test)]
mod tests {
    use super::*;
    /// Check that nil parses properly in sexpr, tuple and record forms
    #[test]
    fn nil_parses() {
        let (rest_sexpr, sexpr) = parse_expr("()").unwrap();
        let (rest_tuple, tuple) = parse_expr("[]").unwrap();
        let (rest_record, record) = parse_expr("{}").unwrap();
        assert_eq!(rest_sexpr, "");
        assert_eq!(rest_tuple, "");
        assert_eq!(rest_record, "");
        assert_eq!(sexpr, Value::sexpr_nil().into());
        assert_eq!(tuple, Value::tuple_nil().into());
        assert_eq!(record, Value::record_nil().into());
    }
    /// Check that nested nils are normalized *in* parser (for efficiency), including mixed ones
    #[test]
    fn nested_nil_normalizes_in_parser() {
        assert_eq!(
            parse_expr("((()))"),
            Ok(("", Value::sexpr_nil().into()))
        );
        assert_eq!(
            parse_expr("[([([()])])]"),
            Ok(("", Value::sexpr_nil().into()))
        );
        assert_eq!(
            parse_expr("[([([[]])])]"),
            Ok(("", Value::tuple_nil().into()))
        );
        assert_eq!(
            parse_expr("(([[(({}))]]))"),
            Ok(("", Value::record_nil().into()))
        );
    }
    /// Check that free symbols parse properly
    #[test]
    fn free_symbols_parse() {
        let test_symbols = &[
            // Simple idents
            "hello", "goodbye", "A", "F", "IDENT",
            // Arithmetic
            "+", "-", "*", "/",
            // Decimal, IP
            "100.324", "192.168.122.1",
            // Punctuated
            "hy-phen-ated", "per.i.o.dic", "under_score",
            // Alphanumeric
            "hey123", "123hey", "123abc123abc"
        ];
        for symbol in test_symbols.iter().copied() {
            assert_eq!(
                parse_expr(symbol),
                Ok(("", Expr::sym(symbol)))
            )
        }
    }
    /// Check that small integers parse properly
    #[test]
    fn small_integers_parse() {
        let test_integers = &[
            // One digit decimal
            (0, "0"), (1, "1"), (5, "5"),
            // Many digit decimal
            (123, "123"), (123, "0123"), (123, "00123"), (1230, "1230"), (10340, "0010340"),
            // One digit hex
            (5, "0x5"), (10, "0xA"), (15, "0xF"),
            // Many digit hex
            (0xCAFE, "0xCAFE"), (5, "0x000005"), (0x50, "0x050")
        ];
        for (integer, symbol) in test_integers.iter().copied() {
            assert_eq!(
                parse_expr(symbol),
                Ok(("", Value::small(integer).into()))
            )
        }
    }
    /// Check simple tuples parse properly
    #[test]
    fn simple_tuples_parse() {
        assert_eq!(
            parse_expr("[[] () {} 1 x]"),
            Ok(("", Value::Tuple(vec![
                Value::tuple_nil().into(),
                Value::sexpr_nil().into(),
                Value::record_nil().into(),
                Value::small(1).into(),
                Expr::sym("x")
                ].into()).into()))
        )
    }
    /// Check tuples with specials inside parse properly
    #[test]
    fn special_tuples_parse() {
        assert_eq!(
            parse_expr("[ #universe #universe # universe ]"),
            Ok(("", Value::Tuple(vec![
                Expr::root_universe(), Expr::root_universe(), Expr::root_universe()
                ].into()).into()))
        );
        assert_eq!(
            parse_expr("[ #get hey man #universe # universe ]"),
            Ok(("", Value::Tuple(vec![
                Get{
                    field: "hey".into(),
                    source: Expr::sym("man").into()
                }.into(),
                Expr::root_universe(), Expr::root_universe()
                ].into()).into()))
        )
    }
    /// Check that simple records parse properly
    #[test]
    fn simple_records_parse() {
        assert_eq!(
            parse_expr("#record nil {}"),
            Ok(("", Value::Record(Record{
                name: Some("nil".into()),
                members: Dictionary::nil()
            }).into()))
        )
    }
    /// Check that simple arithmetic parses properly
    #[test]
    fn simple_arithmetic_parses() {
        let (rest, expr) = parse_expr("(+ 1 (* 2 (+ (/ 5 5) (/ 100 3))))").unwrap();
        let desired: Expr = Value::Sexpr(vec![
            Expr::sym("+"), Value::small(1).into(),
            Value::Sexpr(vec![
                Expr::sym("*"), Value::small(2).into(),
                Value::Sexpr(vec![
                    Expr::sym("+"),
                    Value::Sexpr(vec![
                        Expr::sym("/"),
                        Value::small(5).into(),
                        Value::small(5).into()
                        ].into()).into(),
                    Value::Sexpr(vec![
                        Expr::sym("/"),
                        Value::small(100).into(),
                        Value::small(3).into()
                        ].into()).into()
                    ].into()).into(),
                ].into()).into()
            ].into()).into();
        assert_eq!(rest, "");
        assert_eq!(expr, desired);
    }
    /// Check that field getting parses and parser-normalizes properly for simple cases
    #[test]
    fn simple_get_normalizes_in_parser() {
        assert_eq!(
            parse_expr("#get hello { hello: true goodbye: false }"),
            Ok(("", Expr::sym("true")))
        );
        assert_eq!(
            parse_expr("#get goodbye #record assignment { hello: true goodbye: false }"),
            Ok(("", Expr::sym("false")))
        );
        assert_eq!(
            parse_expr("#get none #record { hello: true goodbye: false none: some }"),
            Ok(("", Expr::sym("some")))
        );
        assert_eq!(
            parse_expr("#get none #record { hello: true goodbye: false none: some }"),
            Ok(("", Expr::sym("some")))
        );
        assert_eq!(
            parse_expr("#get root #struct { root: #universe }"),
            Ok(("", Expr::root_universe()))
        );
        assert_eq!(
            parse_expr("#get 1024 some_struct"),
            Ok(("",
                Get { field : "1024".into(), source : Expr::sym("some_struct").into() }.into()))
        );
    }
    /// Check that nested field getting parses and parser-normalizes properly for simple cases
    #[test]
    fn nested_get_normalizes_in_parser() {
        assert_eq!(
            parse_expr("#get level_2 #get level_1 { level_1 : #record nested { level_2 : yay } }"),
            Ok(("", Expr::sym("yay")))
        );
    }
    /// Check that simple inductive types parse properly
    #[test]
    fn simple_inductive_parses() {
        assert_eq!(
            parse_expr("#inductive Nat {
                z: Nat
                succ: #pi {_: Nat} Nat
            }"),
            Ok(("", Value::Inductive(Inductive {
                is_data: false,
                name: Some("Nat".into()),
                constructors: Dictionary {
                    map: DictionaryMap::from_iter(vec![
                        ("z".into(), Expr::sym("Nat")),
                        ("succ".into(), Value::Pi(Pi{
                            params : Parameters { symbols : vec![
                                TypedSymbol::anon(Expr::sym("Nat"))
                                ] },
                            ty: Expr::sym("Nat").into()
                        }).into())
                    ])
                }
            }).into()))
        )
    }
}
