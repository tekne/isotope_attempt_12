/*!
The isotope parser's symbol table
*/
use internship::IStr;
use hashbrown::hash_map::HashMap;
use smallvec::SmallVec;

use crate::context::ValId;

const INLINE_DEF_CAPACITY: usize = 1;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct SymbolTable {
    symbols: HashMap<IStr, SmallVec<[(ValId, usize); INLINE_DEF_CAPACITY]>>,
    def_stack_depth: usize
}

impl SymbolTable {
    pub fn with_capacity(n: usize) -> SymbolTable {
        SymbolTable {
            symbols: HashMap::with_capacity(n),
            def_stack_depth: 0
        }
    }
    /// Get the truncation index for the definition passed in
    #[inline]
    fn truncation_ix(definition: &[(ValId, usize)], d: usize)
    -> usize {
        for (i, l) in definition.iter().map(|(_, l)| *l).enumerate().rev() {
            if l <= d { return i + 1 }
        }
        0
    }
    /// Get the definition of a given symbol, truncating
    #[inline]
    pub fn get_t(&mut self, name: &IStr) -> Option<ValId> {
        let definition = self.symbols.get_mut(name)?;
        let ix = Self::truncation_ix(definition, self.def_stack_depth);
        definition.truncate(ix);
        if ix > 0 {
            if definition[ix - 1].1 == self.def_stack_depth { Some(definition[ix - 1].0) }
            else { None }
        } else { None }
    }
    /// Get the definition of a given symbol, without truncating
    #[inline]
    pub fn get(&self, name: &IStr) -> Option<ValId> {
        let definition = self.symbols.get(name)?;
        let ix = Self::truncation_ix(definition, self.def_stack_depth);
        if ix > 0 {
            if definition[ix - 1].1 == self.def_stack_depth { Some(definition[ix - 1].0) }
            else { None }
        } else { None }
    }
    /// Undefine the given top stack depth from the definition passed in.
    #[inline]
    fn undef_def(definition: &mut SmallVec<[(ValId, usize); INLINE_DEF_CAPACITY]>, d: usize)
    -> Option<ValId> {
        definition.truncate(Self::truncation_ix(definition, d));
        if let Some(top) = definition.last() {
            if top.1 == d {
                Some(definition.pop().expect("Last confirmed to exist!").0)
            } else {
                None
            }
        } else {
            None
        }
    }
    /// Define a new symbol at the current stack depth.
    /// If one was already defined, return it
    pub fn define(&mut self, name: IStr, val: ValId) -> Option<ValId> {
        let definition = self.symbols.entry(name).or_insert(SmallVec::new());
        let res = Self::undef_def(definition, self.def_stack_depth);
        definition.push((val, self.def_stack_depth));
        res
    }
    /// Delete a given symbol at the current stack depth.
    /// Return its current definition, if it was not already undefined.
    pub fn undef(&mut self, name: IStr) -> Option<ValId> {
        let definition = self.symbols.entry(name.clone()).or_insert(SmallVec::new());
        let res = Self::undef_def(definition, self.def_stack_depth);
        if definition.len() == 0 {
            self.symbols.remove(&name);
        }
        res
    }
    /// Pop a level off the definition stack
    pub fn pop(&mut self) { self.def_stack_depth -= 1 }
    /// Push a level onto the definition stack
    pub fn push(&mut self) { self.def_stack_depth += 1 }
}
