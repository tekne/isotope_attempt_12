use std::fmt::{self, Formatter, Display};
use internship::IStr;
use super::{
    Value,
    Sexpr,
    Tuple,
    Lambda,
    Pi,
    Sigma,
    Inductive,
    Record,
    Struct,
    Finite,
    BitVector,
    ByteVector,
    Universe,
    Projection,
    Get,
    Id,
    Refl,
    Inductor,
    Define
};

/// An isotope expression AST
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Expr {
    /// The type of this expression, if any is asserted
    pub ty: Option<Box<Expr>>,
    /// The value of this expression
    pub value: Value
}

impl Expr {
    /// Create a free symbol expression
    pub fn sym<N: Into<IStr>>(name: N) -> Expr {
        Expr { value : Value::sym(name.into()), ty : None }
    }
    // Create a root universe expression, i.e. a universe not (yet) a subtype of any other
    pub fn root_universe() -> Expr {
        Expr { value : Value::Universe(Universe), ty : None }
    }
}

impl From<Value> for Expr {
    //TODO: limited type inference at this stage
    fn from(value: Value) -> Expr {
        match value {
            value @ Value::Symbol(_) | value @ Value::Natural(_) | value @ Value::SmallNatural(_)
                => Expr { ty : None, value },
            Value::Sexpr(v) => v.into(),
            Value::Tuple(v) => v.into(),
            Value::Lambda(v) => v.into(),
            Value::Pi(v) => v.into(),
            Value::Sigma(v) => v.into(),
            Value::Inductive(v) => v.into(),
            Value::Record(v) => v.into(),
            Value::Struct(v) => v.into(),
            Value::Finite(v) => v.into(),
            Value::BitVector(v) => v.into(),
            Value::ByteVector(v) => v.into(),
            Value::Universe(v) => v.into(),
            Value::Projection(v) => v.into(),
            Value::Get(v) => v.into(),
            Value::Id(v) => v.into(),
            Value::Refl(v) => v.into(),
            Value::Inductor(v) => v.into(),
            Value::Define(d) => d.into()
        }
    }
}

impl Display for Expr {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "{}", self.value)?;
        if let Some(ty) = &self.ty { write!(fmt, ": {}", ty) } else { Ok(()) }
    }
}

impl From<Sexpr> for Expr {
    #[inline]
    fn from(value: Sexpr) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<Tuple> for Expr {
    #[inline]
    fn from(value: Tuple) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<Lambda> for Expr {
    #[inline]
    fn from(value: Lambda) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<Pi> for Expr {
    #[inline]
    fn from(value: Pi) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<Sigma> for Expr {
    #[inline]
    fn from(value: Sigma) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<Inductive> for Expr {
    #[inline]
    fn from(value: Inductive) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<Record> for Expr {
    #[inline]
    fn from(value: Record) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<Struct> for Expr {
    #[inline]
    fn from(value: Struct) -> Self {
        Expr { ty: None, value: value.into() }
}
}

impl From<Finite> for Expr {
    #[inline]
    fn from(value: Finite) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<BitVector> for Expr {
    #[inline]
    fn from(value: BitVector) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<ByteVector> for Expr {
    #[inline]
    fn from(value: ByteVector) -> Self {
        Expr { ty: None, value: value.into() }
}
}

impl From<Universe> for Expr {
    #[inline]
    fn from(value: Universe) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<Projection> for Expr {
    #[inline]
    fn from(value: Projection) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<Get> for Expr {
    #[inline]
    fn from(value: Get) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<Id> for Expr {
    #[inline]
    fn from(value: Id) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<Refl> for Expr {
    #[inline]
    fn from(value: Refl) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<Inductor> for Expr {
    #[inline]
    fn from(value: Inductor) -> Self {
        Expr { ty: None, value: value.into() }
    }
}

impl From<Define> for Expr {
    #[inline]
    fn from(value: Define) -> Self {
        Expr { ty: None, value: value.into() }
    }
}
