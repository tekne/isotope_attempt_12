/*!
The AST for isotope, with associated node kinds and `Display` implementations, as well
as some utilities.
*/

use std::fmt::{self, Formatter, Display};
use internship::{IStr, IBytes};
use bitvec::vec::BitVec;
use num::BigUint;

use crate::TupleIx;

mod expr;
pub use expr::*;

/// The map type used to store dictionary members
pub type DictionaryMap<K, V> = hashbrown::hash_map::HashMap<K, V>;
/// The map type used to store constructors for inductive types
pub type ConstructorMap<K, V> = hashbrown::hash_map::HashMap<K, V>;
/// The integer type used for bitvector length
pub type BitVectorInt = u32;

/// A value for an isotope expression
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Value {
    /// A symbol
    Symbol(IStr),
    /// An S-expression. The empty S-expression is nil.
    Sexpr(Sexpr),
    /// A tuple. The empty tuple is (also) nil.
    Tuple(Tuple),
    /// A lambda function, with an optional return type indication
    Lambda(Lambda),
    /// A pi type
    Pi(Pi),
    /// A sigma type
    Sigma(Sigma),
    /// A declaration of an inductive type
    Inductive(Inductive),
    /// A struct value, or record
    Record(Record),
    /// A struct type
    Struct(Struct),
    /// A finite type with a given number of values
    Finite(Finite),
    /// A bitvector. Of type `#finite n -> #finite "bit" 2`, i.e. #bits n
    BitVector(BitVector),
    /// A byte array. Of type `#finite n -> #finite 8 -> #finite "bit" 2`, i.e. #bytes n
    ByteVector(ByteVector),
    /// Compiler supported naturals. Of type `#inductive Nat { z : Nat, succ : Nat }`
    Natural(BigUint),
    /// Compiler suppoerted naturals *or* values of `Finite`
    SmallNatural(FiniteInt),
    /// A polymorphic typing universe
    Universe(Universe),
    /// Project an element out of a tuple
    Projection(Projection),
    /// Get an element of a dictionary-like type (e.g. `struct`, `record`, `inductive`)
    Get(Get),
    /// Get the identity type for a given type
    Id(Id),
    /// Get refl for a given type
    Refl(Refl),
    /// Get the inductor for a given type
    Inductor(Inductor),
    /// A symbol definition
    Define(Define)
}

impl Value {
    /// Get the normalized nil value
    //TODO: consider whether this should be sexpr, tuple, or record
    pub fn nil() -> Value { Value::sexpr_nil() }
    /// Get the sexpr nil value, `()`
    pub fn sexpr_nil() -> Value { Value::Sexpr(List::nil().into()) }
    /// Get the tuple nil value, `[]`
    pub fn tuple_nil() -> Value { Value::Tuple(List::nil().into()) }
    /// Get the record nil value, `{}`
    pub fn record_nil() -> Value { Value::Record(Record::nil()) }
    /// Create a symbol with a given name
    pub fn sym<N: Into<IStr>>(name: N) -> Value { Value::Symbol(name.into()) }
    /// Create a small natural value
    pub fn small(n: FiniteInt) -> Value { Value::SmallNatural(n) }
}

impl Display for Value {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        use Value::*;
        match self {
            Symbol(s) => write!(fmt, "{}", s),
            Sexpr(s) => write!(fmt, "{}", s),
            Tuple(t) => write!(fmt, "{}", t),
            Lambda(l) => write!(fmt, "{}", l),
            Pi(p) => write!(fmt, "{}", p),
            Sigma(s) => write!(fmt, "{}",s),
            Inductive(i) => write!(fmt, "{}", i),
            Record(r) => write!(fmt, "{}", r),
            Struct(s) => write!(fmt, "{}", s),
            Finite(f) => write!(fmt, "{}", f),
            BitVector(b) => write!(fmt, "{}", b),
            ByteVector(b) => write!(fmt, "{}", b),
            Natural(n) => write!(fmt, "{}", n),
            SmallNatural(n) => write!(fmt, "{}", n),
            Universe(u) => write!(fmt, "{}", u),
            Projection(p) => write!(fmt, "{}", p),
            Get(g) => write!(fmt, "{}", g),
            Id(i) => write!(fmt, "{}", i),
            Refl(r) => write!(fmt, "{}", r),
            Inductor(i) => write!(fmt, "{}", i),
            Define(d) => write!(fmt, "{}", d)
        }
    }
}

/// Create a simple `From` implementation for `Value`'s variants
macro_rules! simple_from_for_value {
    ($t:ident) => {
        impl From<$t> for Value {
            #[inline] fn from(v: $t) -> Value { Value::$t(v) }
        }
    }
}

simple_from_for_value! { Sexpr }
simple_from_for_value! { Tuple }
simple_from_for_value! { Lambda }
simple_from_for_value! { Pi }
simple_from_for_value! { Sigma }
simple_from_for_value! { Inductive }
simple_from_for_value! { Record }
simple_from_for_value! { Struct }
simple_from_for_value! { Finite }
simple_from_for_value! { BitVector }
simple_from_for_value! { ByteVector }
simple_from_for_value! { Universe }
simple_from_for_value! { Projection }
simple_from_for_value! { Get }
simple_from_for_value! { Id }
simple_from_for_value! { Refl }
simple_from_for_value! { Inductor }
simple_from_for_value! { Define }


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Sexpr(pub List);

impl Display for Sexpr {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> { write!(fmt, "({})", self.0) }
}

impl From<Vec<Expr>> for Sexpr { fn from(v: Vec<Expr>) -> Sexpr { Sexpr(v.into()) } }
impl From<List> for Sexpr { fn from(l: List) -> Sexpr { Sexpr(l) } }

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Tuple(pub List);

impl Display for Tuple {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> { write!(fmt, "[{}]", self.0) }
}

impl From<Vec<Expr>> for Tuple { fn from(v: Vec<Expr>) -> Tuple { Tuple(v.into()) } }
impl From<List> for Tuple { fn from(l: List) -> Tuple { Tuple(l) } }

/// A map of names to isotope values
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Dictionary {
    /// The map underlying this dictionary
    pub map: DictionaryMap<IStr, Expr>
}

impl Dictionary {
    pub fn nil() -> Dictionary { Dictionary { map : DictionaryMap::new() } }
}

impl Display for Dictionary {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        let mut first = true;
        for (name, element) in self.map.iter() {
            if first { first = false } else { write!(fmt, " ")? }
            write!(fmt, "{} : {}", name, element)?
        }
        Ok(())
    }
}

/// A list of isotope values
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct List {
    /// The elements of this list
    pub elements: Vec<Expr>
}

impl List {
    pub fn nil() -> List { List { elements : Vec::new() } }
}

impl From<Vec<Expr>> for List {
    fn from(elements: Vec<Expr>) -> List { List { elements } }
}

impl Display for List {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        let mut first = true;
        for element in self.elements.iter() {
            if first { first = false } else { write!(fmt, " ")? }
            write!(fmt, "{}", element)?;
        }
        Ok(())
    }
}

/// An optionally typed isotope symbol
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TypedSymbol {
    pub name: IStr,
    pub ty: Option<Box<Expr>>
}

impl TypedSymbol {
    /// A typed symbol of a given type, with name "_"
    pub fn anon<T: Into<Box<Expr>>>(ty: T) -> Self {
        TypedSymbol { name: "_".into(), ty: Some(ty.into()) }
    }
}

impl Display for TypedSymbol {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "{}", self.name)?;
        if let Some(ty) = &self.ty { write!(fmt, ": {}", ty) } else { Ok(()) }
    }
}

/// A set of parameters to an isotope lambda function or pi type
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Parameters {
    pub symbols: Vec<TypedSymbol>
}

impl Display for Parameters {
    fn fmt(&self, fmt: &mut Formatter)  -> Result<(), fmt::Error> {
        let mut first = true;
        for param in self.symbols.iter() {
            if first { first = false } else { write!(fmt, " ")? }
            write!(fmt, "{}", param)?
        }
        Ok(())
    }
}

/// An isotope lambda function
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Lambda {
    /// The (named) arguments of this lambda function
    pub params: Parameters,
    /// The result of this lambda function
    pub result: Box<Expr>
}

impl Display for Lambda {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "#lambda {{{}}} {}", self.params, self.result)
    }
}

/// An isotope pi type, i.e. a dependent function
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Pi {
    /// The (named) arguments of this pi type, if any
    pub params: Parameters,
    /// The result type of this pi type
    pub ty: Box<Expr>
}

impl Display for Pi {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "#pi {{{}}} {}", self.params, self.ty)
    }
}

/// An isotope sigma type, i.e. a dependent product
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Sigma {
    /// The (named) arguments of this sigma type, if any
    pub params: Parameters,
    /// The contained type of this sigma type
    pub ty: Box<Expr>
}

impl Display for Sigma {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "#sigma {{{}}} {}", self.params, self.ty)
    }
}

/// A declaration of an inductive type or family (with an optional name),
/// mapping constructor names to constructor types
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Inductive {
    /// Whether this inductive type is a data declaration or a bona-fide inductive type.
    /// Affects strict positivity and termination checking
    pub is_data: bool,
    /// The optional name of this inductive type
    pub name: Option<IStr>,
    /// The constructors for this (higher) inductive type or family.
    pub constructors: Dictionary
}

impl Display for Inductive {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        let mut first = true;
        let tag = if self.is_data { "data" } else { "inductive" };
        match &self.name {
            Some(name) => write!(fmt, "#{} {} {{", tag, name)?,
            None => write!(fmt, "#{} {{", tag)?
        }
        for (name, constructor) in self.constructors.map.iter() {
            if first { first = false } else { write!(fmt, " ")? }
            write!(fmt, "{} : {}", name, constructor)?;
        }
        write!(fmt, "}}")
    }
}

/// An isotope struct type, optionally named and associating member names to member types
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Struct {
    /// The name of this struct type
    pub name: Option<IStr>,
    /// The members of this struct
    pub members: Dictionary
}

impl Display for Struct {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        match &self.name {
            Some(name) => write!(fmt, "#struct {} {{{}}}", name, self.members),
            None => write!(fmt, "#struct {{{}}}", self.members)
        }
    }
}

/// An isotope struct value, optionally named and associating member names to member types
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Record {
    /// The name of the type this struct value has, if any
    pub name: Option<IStr>,
    /// The members of this struct
    pub members: Dictionary
}

impl Record {
    pub fn nil() -> Record { Record { name : None, members : Dictionary::nil() } }
}

impl Display for Record {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        match &self.name {
            Some(name) => write!(fmt, "#record {} {{{}}}", name, self.members),
            None => write!(fmt, "{{{}}}", self.members)
        }
    }
}

/// The integer type used to describe how many values a finite type can take on
pub type FiniteInt = u128;

/// A finite type with a given number of values, optionally named
/// If named, it is unequal to any types not having the same name *and* number of values,
/// where types means inductive types *or* Finite.
/// If unnamed, it is unequal to any types having a name or a different number of values,
/// where types means inductive types *or* Finite.
/// More efficient shorthand for an inductive type with a given amount of constructors,
/// though both should compile to roughly the same thing in the end (but the latter may
/// take much longer to parse/interpret/compile for large `n`)
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Finite {
    /// The name this finite type has, if any
    pub name: Option<IStr>,
    /// The amount of values this finite type can take on
    pub values: FiniteInt
}

impl Display for Finite {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        match &self.name {
            Some(name) => write!(fmt, "#finite {} {}", name, self.values),
            None => write!(fmt, "#finite {}", self.values)
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct BitVector(pub BitVec);

impl BitVector {
    /// Resize this bitvector to n bits long.
    /// Error if any bits are 1 after n
    pub fn resize(&mut self, n: usize) -> Result<(), ()> {
        if self.0.len() > n && self.0[n..].count_ones() != 0 {
            Err(())
        } else {
            self.0.resize(n, false);
            Ok(())
        }
    }
}

impl Display for BitVector {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        let len = self.0.len();
        write!(fmt, "{}'x", len)?;
        let hex_len = (len + 3) / 4;
        for i in (0..hex_len).rev() {
            let j = i*4;
            let mut hx = self.0[j] as u8;
            hx += self.0.get(j + 1).unwrap_or(false) as u8 * 2;
            hx += self.0.get(j + 2).unwrap_or(false) as u8 * 4;
            hx += self.0.get(j + 3).unwrap_or(false) as u8 * 8;
            write!(fmt, "{:x}", hx)?;
        }
        Ok(())
    }
}

/// An array of bytes
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ByteVector(pub IBytes);

impl Display for ByteVector {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        let mut first = true;
        write!(fmt, "#bytes [")?;
        for byte in self.0.iter() {
            if first { first = false } else { write!(fmt, " ")? }
            write!(fmt, "{:x}", byte)?;
        }
        write!(fmt, "]")
    }
}

/// A polymorphic typing universe
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Universe;

impl Display for Universe {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> { write!(fmt, "#universe") }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Projection {
    pub source: Box<Expr>,
    pub ix: TupleIx
}

impl Display for Projection {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "#proj {} {}", self.ix, self.source)
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Get {
    pub source: Box<Expr>,
    pub field: IStr
}

impl Display for Get {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "#get {} {}", self.field, self.source)
    }
}


#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Id(pub Box<Expr>);

impl Display for Id {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "#id {}", self.0)
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Refl(pub Box<Expr>);

impl Display for Refl {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "#refl {}", self.0)
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Inductor(pub Box<Expr>);

impl Display for Inductor {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "#ind {}", self.0)
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Define {
    pub name: IStr,
    pub val: Box<Expr>
}

impl Display for Define {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "#define {} {}", self.name, self.val)
    }
}
