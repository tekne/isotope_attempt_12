/*!
A parser for a simple, experimental isotope syntax
*/
use crate::TupleIx;
use internship::IStr;

use crate::builder::{
    tuple::TupleTypeError,
    sexpr::EvalTypeError,
    proj::BuildProjError,
    ContextBuilder
};
use crate::context::{
    ValId,
    Context
};

pub mod ast;
use ast::{
    Expr, Value, List,
    Parameters, TypedSymbol,
    Sexpr, Tuple, Projection, Define, Lambda
};
pub mod syntax;
pub mod symbol_table;
use symbol_table::SymbolTable;

/// Standard isotope parser metadata
pub struct ParserMetadata {
    pub name : IStr
}

impl ParserMetadata {
    /// Create parser metadata having the given name
    #[inline]
    pub fn from_name<I: Into<IStr>>(name: I) -> Self { ParserMetadata { name: name.into() } }
}

impl From<ParserMetadata> for IStr {
    fn from(p : ParserMetadata) -> Self { p.name }
}

/// Discard metadata (keep symbol names for *linked* symbols, though)
pub struct DiscardMetadata;

impl From<ParserMetadata> for DiscardMetadata {
    fn from(_ : ParserMetadata) -> Self { DiscardMetadata }
}


/// An isotope parser
pub struct Parser<'a, M = ParserMetadata> {
    /// The underlying builder the parser emits commands to
    builder: ContextBuilder<'a, M>,
    /// Symbol table for this parser
    symbols: SymbolTable
}

impl<'a, M> Parser<'a, M> {
    /// Build a new isotope parser over the given `ContextBuilder` and a given symbol capacity
    pub fn with_capacity(builder: ContextBuilder<'a, M>, n: usize) -> Self {
        Parser {
            symbols: SymbolTable::with_capacity(n),
            builder
        }
    }
    /// Build a new isotope parser over a given `Context` with a given symbol capacity
    pub fn with_ctx(ctx: &'a mut Context<M>, n: usize) -> Self {
        Self::with_capacity(ContextBuilder::new(ctx), n)
    }
    /// Define a symbol with a given name at the current definition stack depth.
    /// If a symbol used to be defined at the depth, return it.
    pub fn define<I: Into<IStr>>(&mut self, name: I, val: ValId) -> Option<ValId> {
        self.symbols.define(name.into(), val)
    }
    /// Resolve a symbol with a given name at the current definition stack depth.
    pub fn resolve(&mut self, name: &IStr) -> Option<ValId> {
        self.symbols.get_t(name)
    }
    /// Borrow the builder underlying this parser
    #[inline] pub fn builder(&self) -> &ContextBuilder<'a, M> { &self.builder }
    /// Mutably borrow the builder underlying this parser
    #[inline] pub fn builder_mut(&mut self) -> &mut ContextBuilder<'a, M> { &mut self.builder }
    /// Borrow the context underlying this parser
    #[inline] pub fn ctx(&self) -> &Context<M> { self.builder.ctx() }
    /// Mutably borrow the context underlying this parser
    #[inline] pub fn ctx_mut(&mut self) -> &mut Context<M> { self.builder.ctx_mut() }
}

/// A semantic error defining an isotope value
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum SemanticError {
    Eval(EvalTypeError),
    IndexOutOfBounds { maximum: TupleIx, proj: TupleIx },
    SymbolUndefined(IStr)
}

impl From<TupleTypeError> for SemanticError {
    #[inline] fn from(t: TupleTypeError) -> Self { match t {} }
}

impl From<EvalTypeError> for SemanticError {
    #[inline] fn from(e: EvalTypeError) -> Self { Self::Eval(e) }
}

impl From<BuildProjError> for SemanticError {
    #[inline] fn from(b: BuildProjError) -> Self { match b {
        BuildProjError::IndexOutOfBounds { maximum, proj }
        => Self::IndexOutOfBounds { maximum, proj }
    } }
}

impl<'a, M> Parser<'a, M> where ParserMetadata: Into<M> {
    /// Parse a list of expressions, returning a vector of value IDs or an error
    fn parse_list(&mut self, list: &List)
        -> Result<Vec<ValId>, SemanticError> {
        let mut res = Vec::new();
        for element in list.elements.iter() {
            match self.parse(element) {
                Ok(id) => res.push(id),
                Err(err) => {
                    //TODO: delete all newly created values
                    return Err(err)
                }
            }
        }
        Ok(res)
    }
    /// Parse the parameters to an expression, returning a vector of value IDs
    pub fn parse_params(&mut self, params: &Parameters) -> Vec<ValId> {
        params.symbols.iter()
            //TODO: typing
            .map(|TypedSymbol {name,..}| {
                let id = self.builder.build_param();
                self.define(name.clone(), id);
                id
            })
            .collect()
            //TODO: think about this return type
    }
    /// Parse an expression, returning a value ID or an error
    pub fn parse(&mut self, expr: &Expr) -> Result<ValId, SemanticError> {
        match &expr.value {
            Value::Sexpr(Sexpr(list)) => {
                let elems = self.parse_list(list)?;
                self.builder.build_sexpr(elems).map_err(|err| err.into())
            },
            Value::Tuple(Tuple(list)) => {
                let elems = self.parse_list(list)?;
                self.builder.build_tuple(elems).map_err(|err| err.into())
            },
            Value::Lambda(Lambda{ params, result }) => {
                // Enter this lambda function's scope
                self.symbols.push();
                let params = self.parse_params(&params);
                let result = self.parse(result)?;
                // Exit this lambda function's scope
                self.symbols.pop();
                //TODO: remove unused
                Ok(self.builder.build_lambda(params, result))
            },
            Value::Pi(p) => {
                let _params = self.parse_params(&p.params);
                unimplemented!()
            },
            Value::Sigma(s) => {
                let _params = self.parse_params(&s.params);
                unimplemented!()
            },
            Value::Inductive(_i) => unimplemented!(),
            Value::Record(_r) => unimplemented!(),
            Value::Struct(_s) => unimplemented!(),
            Value::Finite(_f) => unimplemented!(),
            Value::BitVector(_v) => unimplemented!(),
            Value::ByteVector(_v) => unimplemented!(),
            Value::Universe(_) => Ok(self.builder.build_universe()),
            Value::Projection(Projection{ source, ix }) => {
                //TODO: remove unused
                let source = self.parse(source)?;
                self.builder.build_proj(source, *ix).map_err(|err| err.into())
            },
            Value::Get(_g) => unimplemented!(),
            Value::Id(i) => {
                //TODO: remove unused
                let source = self.parse(&i.0)?;
                Ok(self.builder.build_id(source).into())
            },
            Value::Refl(r) => {
                //TODO: remove unused
                let source = self.parse(&r.0)?;
                Ok(self.builder.build_refl(source).into())
            },
            Value::Inductor(i) => {
                //TODO: remove unused
                let source = self.parse(&i.0)?;
                Ok(self.builder.build_ind(source))
            },
            Value::Symbol(s) => {
                self.resolve(s).ok_or(SemanticError::SymbolUndefined(s.clone()))
            },
            Value::Natural(_n) => unimplemented!(),
            Value::SmallNatural(_n) => unimplemented!(),
            Value::Define(Define{ name, val }) => {
                let val = self.parse(val)?;
                self.define(name.clone(), val);
                Ok(val)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parser::syntax::parse_expr;
    use crate::context::{
        Context,
        Definition,
        Universe
    };
    /// Check sexpr nil parses and normalizes properly
    #[test]
    pub fn sexpr_nil_parses_properly() {
        let (rest, nil) = parse_expr("()").unwrap();
        assert_eq!(rest, "");
        let mut ctx = Context::<DiscardMetadata>::with_capacity(1, 0);
        let mut parser = Parser::with_ctx(&mut ctx, 0);
        let nil_res = parser.parse(&nil);
        let nil = parser.builder().nil().expect("Nil should have been generated");
        assert_eq!(nil_res, Ok(nil))
    }
    /// Check tuple nil parses and normalizes properly
    #[test]
    pub fn tuple_nil_parses_properly() {
        let (rest, nil) = parse_expr("[]").unwrap();
        assert_eq!(rest, "");
        let mut ctx = Context::<DiscardMetadata>::with_capacity(1, 0);
        let mut parser = Parser::with_ctx(&mut ctx, 0);
        let nil_res = parser.parse(&nil);
        let nil = parser.builder().nil().expect("Nil should have been generated");
        assert_eq!(nil_res, Ok(nil))
    }
    /// Check that a tuple of nils parses properly
    #[test]
    pub fn tuple_of_nil_parses_properly() {
        let (rest, tup) = parse_expr("[() () []]").expect("Tuple of nils is syntactically valid");
        assert_eq!(rest, "");
        let mut ctx = Context::<DiscardMetadata>::with_capacity(4, 0);
        let mut parser = Parser::with_ctx(&mut ctx, 0);
        let tup_res = parser.parse(&tup).expect("Tuple of nils is semantically valid");
        let nil = parser.builder().nil().expect("Nil should have been generated");
        if let Definition::Tuple(t) = parser.ctx()[tup_res].def() {
            for elem in t.elements.iter().copied() {
                assert_eq!(elem, nil)
            }
        } else {
            panic!("Tuple of nils is a tuple!")
        }
    }
    /// Check that simple projections parse properly
    #[test]
    pub fn simple_projections_parse_properly() {
        let (rest, nil) = parse_expr("#proj 1 [[[] []] [()] ()]")
            .expect("Projection is syntactically valid");
        assert_eq!(rest, "");
        let mut ctx = Context::<DiscardMetadata>::with_capacity(1, 0);
        let mut parser = Parser::with_ctx(&mut ctx, 0);
        let nil_res = parser.parse(&nil);
        let nil = parser.builder().nil().expect("Nil should have been generated");
        assert_eq!(nil_res, Ok(nil))
    }
    /// Check that simple definitions work properly
    #[test]
    pub fn simple_definitions_work_properly() {
        let mut ctx = Context::<DiscardMetadata>::with_capacity(1, 0);
        let mut parser = Parser::with_ctx(&mut ctx, 0);
        let (rest, x) = parse_expr("#define x [[[[[] []] []]]]")
            .expect("Definition is syntactically valid");
        assert_eq!(rest, "");
        let _x = parser.parse(&x).expect("Definition is semantically valid");
        let (rest, nil) = parse_expr("#proj 1 x").expect("Projection is syntactically valid");
        assert_eq!(rest, "");
        let nil = parser.parse(&nil).expect("Projection is semantically valid");
        assert_eq!(nil, parser.builder().nil().expect("Nil should have been generated"))
    }
    /// Check that universes parse properly
    #[test]
    pub fn universes_work_properly() {
        let mut ctx = Context::<DiscardMetadata>::with_capacity(1, 0);
        let mut parser = Parser::with_ctx(&mut ctx, 0);
        let (rest, u) = parse_expr("#universe").expect("Universes are syntactically valid");
        assert_eq!(rest, "");
        let u = parser.parse(&u).expect("Universes are semantically valid");
        assert_eq!(parser.ctx()[u].def(), &Definition::Universe(Universe))
    }
    /// Check that the identity lambda function parses properly
    #[test]
    pub fn identity_lambda_parses_properly() {
        let mut ctx = Context::<DiscardMetadata>::with_capacity(1, 0);
        let mut parser = Parser::with_ctx(&mut ctx, 0);
        let (rest, id) = parse_expr("#lambda x x").expect("Lambdas are syntactically valid");
        assert_eq!(rest, "");
        let id = parser.parse(&id).expect("The identity is semantically valid");
        match parser.ctx()[id].def() {
            Definition::Lambda(crate::context::Lambda { params, result }) => {
                assert_eq!(params.len(), 1);
                assert_eq!(params[0], *result);
            },
            _ => panic!("The identity is here defined as a lambda function")
        }
    }
}
